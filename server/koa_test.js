const Koa = require("koa");
var bodyParser = require('koa-bodyparser');
var beautify = require('js-beautify').js;
const app = new Koa();
const path = require('path');


app.use(async (ctx, next) => {
    let start = Date.now();
    await next();
    const ms = Date.now() - start;
    console.log(`${ctx.method} ${ctx.url} - ${ms}ms`);

})

app.use(bodyParser());

app.use(async (ctx, next) => {
    //await next();
    console.log(next());
    console.log(beautify(JSON.stringify(ctx.request.body)));
    ctx.response.body = { msg: "成功。", state: 0 };
})
app.listen(3000);
console.log(`Server started.`);
