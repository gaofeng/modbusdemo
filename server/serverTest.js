
// create a tcp modbus client
const modbus = require('jsmodbus')
const SerialPort = require('serialport')
const socket = new SerialPort('COM1', {
  baudRate: 9600
})
const holding = Buffer.alloc(10000)
const rtuServer = new modbus.server.RTU(socket, {
  holding: holding,
  input: Buffer.alloc(1000)
})
rtuServer.on('connection', function () {
  console.log('New Connection')
})

rtuServer.on('readInputRegisters', function (request, send) {
    console.log('readinput')
})

rtuServer.on('readHoldingRegisters', function (request, send) {
    console.log('readhold')
})

rtuServer.on('writeMultipleRegisters', function (request, send) {

  console.log('holding')

})

rtuServer.on('preReadInputRegisters', function (request, send) {
  console.log('prereadinput')
})