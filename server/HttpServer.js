//启动Modbus RTU服务器
const modbus = require('../../node-modbus')
const SerialPort = require('serialport')
const delay = require('delay')
const CAF = require('caf')

const port = new SerialPort('COM2', {
    baudRate: 9600,
    autoOpen: false
})



port.on('open', function () {
    // open logic
    console.log('open event');
    const holding = Buffer.alloc(1000)
    const input = Buffer.alloc(1000)
    const rtuServer = new modbus.server.RTU(port, {
        holding: holding,
        input: input
    })
    rtuServer.input.writeUInt16BE(100, 0);
    rtuServer.on('connection', function () {
        console.log('New Connection')
    })

    /**
     * 写 瞬时齿速
     * @param {*} data 数组，最大10个
     */
    function writeRTPulse(data) {
        data.forEach((item, index) => {
            rtuServer.input.writeUInt16BE(item * 10, 120 + index * 2);
        })
    }

    /**
     * 写 累计齿数
     * @param {*} data 数组，最大10个
     */
    function writeACPulse(data) {
        data.forEach((item, index) => {
            rtuServer.input.writeUInt32BE(item, 4 + index * 4);
        })
    }

    /**
     * 写 时间补偿后的累计齿数
     * @param {*} data 数组，最大10个
     */
    function writeTAPulse(data) {
        data.forEach((item, index) => {
            rtuServer.input.writeUInt32BE(item * 100 + index * 10, 68 + index * 4);
        })
    }

    const S = {
        Stop: 0x03,
        Q3: 0x05,
        Q3End: 0x06,
        Q2: 0x07,
        Q2End: 0x08,
        Q1: 0x09,
        Q1End: 0x0A,
        Drawoff: 0x0C,
        Auto: 0x0D
    }
    /**
     * 写运行状态
     * @param {*} state 
     */
    function writeState(state) {
        rtuServer.holding.writeUInt16BE(state, 0);
    }

    function writeWeight(value) {
        rtuServer.input.writeUInt16BE(value, 2);
    }

    let g_autoPressureResult = false;

    function writeAutoPressureResult(isSuccess) {
        let value = 0;
        if (isSuccess) value = 1;
        rtuServer.input.writeUInt16BE(value, 26 * 2);
    }

    let token;
    let autoProcess = CAF(function* f(signal) {
        signal.pr.catch(reason => {
            console.log(reason);
        });
        console.log('开始自动检定');
        writeRTPulse([70, 71, 72, 71.5, 72, 69, 69.5, 70]);
        writeWeight(0);
        yield CAF.delay(1000);

        writeState(S.Q3);
        yield CAF.delay(1000);

        //writeACPulse([760, 760, 760, 760, 760, 760, 760, 760]);
        writeTAPulse([760, 760, 760, 760, 760, 760, 760, 760]);
        writeWeight(10600);
        writeState(S.Q3End)
        yield CAF.delay(1000);

        writeState(S.Q2)
        yield CAF.delay(1000);

        writeTAPulse([72, 72, 72, 72, 72, 72, 72, 72]);
        writeWeight(11600);
        writeState(S.Q2End)
        yield CAF.delay(1000);

        writeState(S.Q1)
        yield CAF.delay(1000);

        writeTAPulse([70, 70, 70, 70, 70, 70, 70, 70]);
        writeWeight(12600);
        writeState(S.Q1End)
        yield CAF.delay(1000);

        writeState(S.Drawoff);
        yield CAF.delay(1000);

        writeState(S.Stop);
        console.log('自动检定完成!');
    });
    let pressureProcess = CAF(function* f(signal) {
        signal.pr.catch(reason => {
            console.log(reason);
        });
        console.log('开始打压测试...');
        yield CAF.delay(1000);
        writeState(0x11);
        yield CAF.delay(1000);
        writeState(0x12);
        yield CAF.delay(1000);
        writeState(0x13);
        yield CAF.delay(1000);
        writeState(0x14);
        yield CAF.delay(1000);

        g_autoPressureResult = !g_autoPressureResult;
        writeAutoPressureResult(g_autoPressureResult);
        yield CAF.delay(200);
        writeState(0x15);
        console.log('打压结束');
    });
    //postWriteSingleRegister
    rtuServer.on('postWriteSingleRegister', function (request, send) {
        console.log('write single hold reg');
        if (request.body.address == 0) {
            if (request.body.value == S.Auto) {
                token = new CAF.cancelToken()
                autoProcess(token);
            } else if (request.body.value == S.Stop) {
                if (token) {
                    token.abort('Manual stopped.');
                }
            } else if (request.body.value == 0x10) {
                token = new CAF.cancelToken()
                pressureProcess(token);
            }
        }
    })
    rtuServer.on('postWriteMultipleRegisters', function (request, send) {
        console.log('write multi hold reg')
    })
})



// rtuServer.on('readInputRegisters', function (request, send) {
//     console.log('readinput')
// })

// rtuServer.on('readHoldingRegisters', function (request, send) {
//     console.log('readhold')
// })

// rtuServer.on('writeMultipleRegisters', function (request, send) {

//     console.log('holding')

// })

// rtuServer.on('preReadInputRegisters', function (request, send) {
//     if (iosocket) {
//         iosocket.emit('debug', 'prereadinput');
//     }
//     else {
//         console.log("prereadinput");
//     }
// })
//MoDBUS客户端
// const spSocket = new SerialPort('COM2', {
//     baudRate: 9600
// })

// // set Slave PLC ID
// const client = new modbus.client.RTU(spSocket, 1)

// spSocket.on('error', function (err) {
//     console.log(err)
// })

const Koa = require('koa');
const send = require('koa-send');
const fs = require('fs');
const app = new Koa();
const router = require('koa-router')()
const server = require('http').Server(app.callback());
const io = require('socket.io')(server);
var colors = require('colors/safe');

const lsn_port = 8081;

var iosocket;


server.listen(process.env.PORT || lsn_port, () => {
    console.log(`app run at : http://127.0.0.1:${lsn_port}`);
})

app.use(router.routes(), router.allowedMethods())

router.get('/', async function (ctx, next) {
    await send(ctx, './client/index.html');
})

router.get('/main-bundle.min.js', async function (ctx, next) {
    await send(ctx, './client/dist/main-bundle.min.js');
})

io.on('connection', socket => {
    iosocket = socket;
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
    SerialPort.list().then(ports => {
        let portNames = [];
        ports.forEach(port => {
            portNames.push(port.comName);
        });
        console.log(portNames);
        socket.emit('port-list', portNames);
    })
    port.open(err => {
        if (err) {
            console.log(err);
            return;
        }
        console.log('port opened!');
    });
    socket.on('chat message', function (msg) {
        console.log('message: ' + msg);
        socket.emit('debug', colors.red('Success Received:' + msg + '\r\n'));
        // client.readInputRegisters(0, 12).then(function (resp) {
        //     console.log(resp)
        //     spSocket.close()
        // }, function (err) {
        //     console.log(err)
        //     spSocket.close()
        // })
    });
})