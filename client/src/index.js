import $ from 'jquery';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'xterm/dist/xterm.css'
import { Terminal } from 'xterm';

$(function () {
    let socket = io();
    let term = new Terminal({
        'disableStdin': true,
        'cursorBlink': true
    });
    term.open(document.getElementById('xterm-container'));
    
    term.onKey(function(key){
        socket.emit('chat message', key.key);
    })
    
    $('form').submit(function (e) {
        e.preventDefault(); // prevents page reloading
        socket.emit('chat message', $('#m').val());
        $('#m').val('');
        return false;
    });
    socket.on('debug', function (msg) {
        term.write(msg);
    })
    socket.on('port-list', function (data) {
        let serialList = $('#serialList');
        data.forEach(name => {
            let opt = $('<option>').html(name);

            serialList.append(opt);
        });
    });

    

});